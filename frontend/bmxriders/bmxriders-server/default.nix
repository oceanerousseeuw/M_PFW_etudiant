{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") { } }:

let

  app = pkgs.haskellPackages.callCabal2nix "bmxriders-server" ./. {};

  pkg = pkgs.stdenv.mkDerivation {
    name = "bmxriders-server-pkg";
    src = ./.;
    buildInputs = [ app ];
    installPhase = ''
      mkdir $out
      cp ${app}/bin/* $out/
      cp -R static $out/
    '';
  };

  env = app.env;

in

if pkgs.lib.inNixShell then env else pkg

