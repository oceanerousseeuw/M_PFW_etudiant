{ pkgs ? import <nixpkgs> {} }:

let

  drv = pkgs.haskellPackages.callCabal2nix "bmxriders" ./. {};

  drv2 = pkgs.stdenv.mkDerivation {
    name = "bmxriders";
    src = ./.;
    buildInput = [
      drv
    ];
    installPhase = ''
      mkdir -p $out
      cp ${drv}/bin/bmxriders $out/
      cp -R img $out/
      cp *.sql $out/
    '';
  };

in

if pkgs.lib.inNixShell then drv.env else drv2

