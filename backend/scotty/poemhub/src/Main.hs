{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Trans (liftIO)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty (get, middleware, param, post, rescue, scotty, html)

import qualified Model
import qualified View

-- TODO implement routes

main = scotty 3000 $ do

  middleware logStdoutDev

  let fakePoems = [ Model.Poem 1 "author1" "title1" 2001 "body1"
                    , Model.Poem 2 "author2" "title2" 2002 "body2" ]

  get "/" $ do
    html $ View.mkpage "Poem hub - Home" $ View.homeRoute fakePoems

    --page de lecture du body d'un poeme
  get "/read" $ do
    id <- param id
    html $ View.mkpage "Poem hub - Read" $ View.readRoute ( Model.Poem 1 "author1" "title1" 2001 "body1")
    --page d'ecriture d'un nouveau poeme
  --get "/write" $ do
   -- html $ View.mkpage "Poem hub - Write" $ View.homeRoute fakePoems  