{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Maybe (isNothing, fromJust)
import Lucid
import System.Environment (getEnv)
import Text.Read (readMaybe)
import Web.Scotty (get, param, scotty, html, rescue)

import Mymath (square)

main :: IO ()
main = do
    portStr <- getEnv "PORT"
    let port = read portStr

    scotty port $ do
        get "/" $ do
            value <- param "value" `rescue` (\ _ -> return "")
            html $ renderText $ html_ $ body_ $ do
                h1_ "Mymath"
                form_ [action_ "/", method_ "get"] $ do
                    "Enter an integer: "
                    input_ [name_ "value", value_ value]
                    input_ [type_ "submit"]
                let vMay = readMaybe (T.unpack value) :: Maybe Int
                    v = fromJust vMay
                    s = square v
                    r = if isNothing vMay
                        then ""
                        else concat ["the square of ", show v, " is ", show s]
                p_ $ toHtml r 

