{-# LANGUAGE OverloadedStrings #-}

-- import qualified Clay as C
import qualified Data.Text.IO as TIO
--import qualified Data.Text.Lazy as L
import Lucid
import qualified Data.Text as T
import qualified Database.PostgreSQL.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)

data Music = Music
    { _title :: T.Text
    , _artist :: T.Text
    } deriving (Show)

instance FromRow Music where 
    fromRow = Music <$> field <*> field


createHtmlFile :: [Music] -> Html()
createHtmlFile content = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "My List"
            ul_ $ do
                createLines content

createLines :: [Music] -> Html()
createLines musics = do
    mapM_ (li_ . toHtml . formatTitle) musics
    where formatTitle (Music t a) = T.concat [t," - ",a]

main :: IO ()
main = do
    conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=mydbhtml user=toto password='toto'"

    res <- SQL.query_ conn "SELECT titles.name as title, artists.name as artist FROM titles, artists WHERE titles.artist = artists.id" :: IO [Music]
    print res
    renderToFile "liste.html" $ createHtmlFile res

    SQL.close conn
