{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "timezone_client" ./. {};
in
if pkgs.lib.inNixShell then drv.env else drv

