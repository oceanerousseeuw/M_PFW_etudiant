{-# LANGUAGE OverloadedStrings #-}

-- import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
--import qualified Data.Text.Lazy as L
import           Lucid
import           System.Environment (getArgs, getProgName)

createHtmlFile :: [T.Text] -> Html()
createHtmlFile content = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "My List"
            ul_ $ do
                createLines content

createLines :: [T.Text] -> Html()
createLines content = do
    mapM_ (li_ . toHtml) content


main :: IO ()
main = do
    args <- getArgs
    if length args /= 2
    then do
        progName <- getProgName
        putStrLn $ "usage: " ++ progName ++ " <input dat> <output html>"
    else do
        x <- getArgs
        let [input,output] = x
        file <- TIO.readFile input
        let linesResult = T.lines file
        renderToFile output $ createHtmlFile linesResult
