"use strict";

const express = require("express");
const app = express();
const fs = require("fs");

app.get("/api", function (req, res) {
   fs.readFile(__dirname + "/bmxriders.json", 
       "utf8", 
       function (err, data) {
           res.setHeader("Content-Type", "application/json");
           res.send(data);
       });
});

app.use("/", express.static(__dirname + "/static"));

const port = 3000;
const server = app.listen(port, function () {
  console.log(`Listening on port ${port}...`);
});

/*
node2nix -8 --development
nix-shell -A shell
node app.js
*/

