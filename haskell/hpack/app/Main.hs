{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.IO as TIO
import qualified Data.Text as T
import           System.Environment (getArgs)

--main :: IO ()
-- main =  getArgs 
--    >>= readFile . head 
--    >>= (mapM_ putStrLn) . lines

main = do
    if length args /= 3
        then do
          putStrLn $ concat ["usage: ", progName, " <outfile> <size> <steps>"]
          putStrLn $ concat ["e.g.: ", progName, " out.pnm 400 10000"]
    else
        x <- getArgs
        let headVal = head x
        file <- TIO.readFile headVal
        let linesResult = T.lines file
        (mapM_ TIO.putStrLn) linesResult