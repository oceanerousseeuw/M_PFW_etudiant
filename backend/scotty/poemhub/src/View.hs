{-# LANGUAGE OverloadedStrings #-}

module View (mkpage, homeRoute, readRoute) where

import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid

import qualified Model

-- TODO implement CSS styles
myCss :: C.Css
myCss = do
    C.a C.# C.byClass "aCss" C.? do
        C.textDecoration  C.none
        C.color           C.inherit
    C.body C.? do
        C.backgroundColor  C.azure
    C.div C.# C.byClass "divCss" C.? do
        C.backgroundColor  C.beige
        C.border           C.solid (C.px 1) C.black
        C.margin           (C.em 1) (C.em 1) (C.em 1) (C.em 1)
        C.width            (C.px 320)
        C.textAlign        C.center
        C.float            C.floatLeft
    C.img C.# C.byClass "imgCss" C.? do
        C.width            (C.px 320)
        C.height           (C.px 240)
    C.p C.# C.byClass "pCss" C.? do
        C.fontWeight C.bold

-- TODO implement route views

mkpage :: Lucid.Html () -> Lucid.Html () -> L.Text
mkpage titleStr page = renderText $ html_ $ do
  head_ $ do
    title_ titleStr
    style_ $ L.toStrict $ C.render $ myCss
  body_ page

homeRoute :: [Model.Poem] -> Lucid.Html ()
homeRoute poems = do
  h1_ "Poem hub"
  div_ $ mapM_ onePoemtoHtml poems

onePoemtoHtml :: (Model.Poem) -> Lucid.Html()
onePoemtoHtml (Model.Poem id author title year body) =
  div_ [class_ "divCss"] $ do
    a_ [class_ "aCss", href_ $ T.concat ["read", (Model.getIdText (Model.Poem id author title year body))]] $ do
        p_ [class_ "pCss"] $ toHtml title
        p_ $ toHtml $ T.concat ["by ", author, " (", (Model.getYearText (Model.Poem id author title year body)), ")"]

readRoute :: Model.Poem -> Lucid.Html ()
readRoute (Model.Poem id author title year body) = do
    div_ $ do
        p_ $ toHtml $ T.concat [title, " by ", author, " (", (Model.getYearText (Model.Poem id author title year body)), ")"]
