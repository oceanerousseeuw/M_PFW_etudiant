{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.PostgreSQL.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)

data Name = Name T.Text deriving Show

instance FromRow Name where
    fromRow = Name <$> field 

main :: IO ()
main = do
    conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=mydb2 user=toto3 password='toto3'"

    putStrLn "\n*** id et nom des artistes ***"
    res1 <- SQL.query_ conn "SELECT id, name FROM artists" :: IO [(Int, T.Text)]
    print res1

    putStrLn "\n*** id et nom de l'artiste 'Radiohead' ***"
    res2 <- SQL.query conn "SELECT id, name FROM artists WHERE name=?" 
        (SQL.Only $ T.pack "Radiohead")
        :: IO [(Int, T.Text)]
    print res2

    putStrLn "\n*** tous les champs des titres dont le nom contient 'ust' et dont l'id > 1 ***"
    res3 <- SQL.query conn "SELECT * FROM titles WHERE name LIKE ? AND id > ?" 
        ("%ust%"::T.Text, 1::Integer)
        :: IO [(Int, Int, T.Text)]
    print res3

    putStrLn "\n*** noms des titres (en utilisant le type Name) ***"
    res4 <- SQL.query_ conn "SELECT name FROM titles" 
        :: IO [Name]
    print res4

    putStrLn "\n*** noms des titres (en utilisant une liste de T.Text) ***"
    res5 <- SQL.query_ conn "SELECT name FROM titles" 
        :: IO [[T.Text]]
    print res5

    SQL.close conn

