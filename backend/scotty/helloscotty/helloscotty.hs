{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Web.Scotty (scotty, get, post, html, param, rescue, redirect)
import Lucid
import Control.Monad.IO.Class (liftIO)
import Control.Concurrent (newMVar, modifyMVar_, readMVar)

createhomepage:: Html()
createhomepage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ $ "Hello, Home page"
            a_ [href_  "http://localhost:3000/hello"] "Aller à la page hello"
            
            

--donner bon type dans la signature
--createhellopage::  -> Html()
createhellopage myName = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ $ toHtml $ T.concat["Hello ",myName]
            form_ [action_ "/hello", method_ "post"] $ do
                input_ [name_ "name", value_ "enter your name here"]
                input_ [type_ "submit"]
            a_ [href_  "http://localhost:3000/"] "Aller à la home page"

main = scotty 3000 $ do
    get "/" $ html $ renderText $ createhomepage
        -- http://localhost:3000/

    get "/hello" $ do
        html $ renderText $ createhellopage ""
        -- http://localhost:3000/hello

    post "/hello" $ do
        txt <- param "name"
        html $ renderText $ createhellopage txt
        ---- http://localhost:3000/hello